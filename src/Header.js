import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const Wrapper = styled('header')`
  height: 80px;

  background-color: #6cc7be;
  color: white;

  div {
    display: flex;
  }

  h3 {
    margin: auto 0;
    line-height: 0;
  }
`;

const Header = ({ title }) => (
  <Wrapper>
    <div className="inner-wrapper">
      <h3>{title}</h3>
    </div>
  </Wrapper>
);

Header.propTypes = {
  title: PropTypes.string.isRequired
};

export default Header;
