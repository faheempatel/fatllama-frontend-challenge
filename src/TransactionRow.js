import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import { format } from 'date-fns';

import fetchUserInfo from './lib/fetchUserInfo';

import chevronRight from './assets/chevron-right.svg';
import chevronDown from './assets/chevron-down.svg';

import DetailsMenu from './DetailsMenu';
import StatusLabel from './StatusLabel';

const Wrapper = styled('div')`
  :nth-child(odd) {
    background: #f9f9f9;
  }
`;

const Row = styled('div')`
  display: flex;
  align-items: center;
  width: 100%;
  padding: 8px;
  > p {
    width: calc(100% / 6);
  }
`;

const Chevron = styled('div')`
  margin-top: 2px;
  align-self: center;
  width: 30px;
  :hover {
    cursor: pointer;
  }
`;

class TransactionRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      lender: null,
      borrower: null,
      status: this.props.transaction.status
    };
  }

  static propsTypes = {
    transaction: PropTypes.object.isRequired
  };

  fetchUsers = () => {
    const lender = fetchUserInfo({ userId: this.props.transaction.lenderId });
    const borrower = fetchUserInfo({
      userId: this.props.transaction.lenderId
    });
    Promise.all([lender, borrower]).then(
      data => {
        this.setState({ lender: data[0], borrower: data[1] });
      },
      err => {
        // Try again on failure
        // Warning: Potential infinite loop if request keeps failing
        this.fetchUsers();
      }
    );
  };

  onClick = () => {
    this.setState({ open: !this.state.open });

    if (!this.state.lender || !this.state.borrower) {
      this.fetchUsers();
    }
  };

  renderChevron() {
    if (this.state.open) {
      return <img src={chevronDown} alt={'Chevron facing down'} />;
    } else {
      return <img src={chevronRight} alt={'Chevron facing right'} />;
    }
  }

  render() {
    const { id, itemId, fromDate, toDate } = this.props.transaction;

    return (
      <Wrapper>
        <Row>
          <Chevron onClick={this.onClick}>{this.renderChevron()}</Chevron>
          <p>{id}</p>
          <p>{itemId}</p>
          <p>{format(fromDate, 'DD/MM/YYYY')}</p>
          <p>{format(toDate, 'DD/MM/YYYY')}</p>
          <StatusLabel status={this.state.status} />
        </Row>

        <DetailsMenu
          open={this.state.open}
          lender={this.state.lender}
          borrower={this.state.borrower}
          status={this.state.status}
          {...this.props}
        />
      </Wrapper>
    );
  }
}

export default TransactionRow;
