import React from 'react';
import PropTypes from 'prop-types';

const Dropdown = ({ children }) => <select>{children}</select>;

Dropdown.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

export default Dropdown;
