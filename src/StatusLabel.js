import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const COLORS = {
  PRE_AUTHORIZED: 'red',
  PRE_AUTHORIZED_CANCELLED: 'lightgray',
  CANCELLED: 'lightgray',
  ESCROW: 'orange',
  FL_APPROVED: 'green',
  PAID: 'green'
};

const Wrapper = styled('div')`
  font-weight: bold;
  color: ${props => props.color};
`;

const StatusLabel = ({ status, priority }) => {
  const color = COLORS[status.toUpperCase()];
  return (
    <Wrapper color={color}>
      <p>{status}</p>
    </Wrapper>
  );
};

StatusLabel.propTypes = {
  status: PropTypes.string.isRequired,
  prority: PropTypes.oneOf(['low', 'medium', 'high'])
};

export default StatusLabel;
