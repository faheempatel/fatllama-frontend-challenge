import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const Wrapper = styled('button')`
  padding: 16px;

  border: none;
  border-radius: 4px;
  background: ${props =>
    props.backgroundColor ? props.backgroundColor : 'initial'};

  font-weight: bold;
  font-size: 16px;
  color: ${props => (props.textColor ? props.textColor : 'initial')};
`;
const Button = props => <Wrapper {...props}>{props.text}</Wrapper>;

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;
