import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

import { cancelTransaction, approveTransaction } from './lib/updateTransaction';

import UserBlock from './UserBlock';
import PriceRow from './PriceRow';
import Button from './Button';

const Menu = styled('div')`
  display: ${props => (props.open ? 'flex' : 'none')};
  flex-wrap: wrap;

  padding-top: 16px;
  background: #fff;
  border-bottom: 2px solid #e5e5e4;

  .user-info {
    width: 30%;
    margin-right: 16px;
    margin-bottom: 16px;

    border-right: 1px solid #e8e8e8;

    > :last-child {
      margin-top: 24px;
    }
  }
`;

const ButtonsRow = styled('div')`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  padding: 24px;
  border-top: 1px solid #e8e8e8;
  button:first-of-type {
    margin-right: 8px;
  }
`;

class DetailsMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { status: this.props.status };
  }

  static propTypes = {
    open: PropTypes.bool.isRequired,
    lender: PropTypes.object,
    borrower: PropTypes.object,
    status: PropTypes.string.isRequired,
    transaction: PropTypes.object.isRequired
  };

  static defaultProps = {
    lender: {},
    borrower: {}
  };

  onCancel = () => {
    cancelTransaction({ id: this.props.transaction.id }).then(() => {
      this.setState({ status: 'CANCELLED' });
    });
  };

  onApprove = () => {
    approveTransaction({ id: this.props.transaction.id }).then(() => {
      this.setState({ status: 'FL_APPROVED' });
    });
  };

  render() {
    return (
      <Menu open={this.props.open}>
        <div className="user-info">
          {this.props.lender && (
            <UserBlock type={'lender'} {...this.props.lender} />
          )}
          {this.props.borrower && (
            <UserBlock type={'borrower'} {...this.props.borrower} />
          )}
        </div>

        <PriceRow {...this.props.transaction} />

        {this.state.status === 'PRE_AUTHORIZED' ||
          (this.state.status === 'ESCROW' && (
            <ButtonsRow>
              <Button
                text={'Cancel Transaction'}
                onClick={this.onCancel}
                backgroundColor={'#EB5757'}
                textColor={'#FFF'}
              />
              <Button
                text={'Approve'}
                onClick={this.onApprove}
                backgroundColor={'#27AE60'}
                textColor={'#FFF'}
              />
            </ButtonsRow>
          ))}
      </Menu>
    );
  }
}

export default DetailsMenu;
