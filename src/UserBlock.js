import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const Wrapper = styled('div')`
  display: flex;
  align-items: flex-end;

  p {
    line-height: 1.4;
  }

  img {
    width: 80px;
    height: 80px;
    margin-top: 8px;
    margin-right: 16px;
  }

  .user-type {
    text-transform: capitalize;
    font-size: 14px;
    color: #bfbfc3;
  }

  .user-name {
    font-weight: bold;
  }
`;

const UserBlock = ({
  type,
  firstName,
  lastName,
  email,
  telephone,
  profileImgUrl
}) => {
  const fullName = `${firstName} ${lastName || ''}`.trim();
  return (
    <Wrapper>
      <div>
        <p className="user-type">{type}</p>
        <img src={profileImgUrl} alt={fullName} />
      </div>
      <div>
        <p className="user-name">{fullName}</p>
        <p className="user-email">{email}</p>
        <p className="user-phone">{telephone}</p>
      </div>
    </Wrapper>
  );
};

UserBlock.propTypes = {
  type: PropTypes.oneOf(['lender', 'borrower']).isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string,
  email: PropTypes.string.isRequired,
  telephone: PropTypes.string.isRequired,
  profileImgUrl: PropTypes.string.isRequired
};

export default UserBlock;
