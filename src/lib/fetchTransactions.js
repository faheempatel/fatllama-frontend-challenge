import 'whatwg-fetch';

export default ({ page }) => {
  const url = `http://localhost:8080/transactions/${page}`;
  return fetch(url)
    .then(response => {
      if (!response.ok) throw new Error('Request Failed');
      return response;
    })
    .then(data => data.json());
};
