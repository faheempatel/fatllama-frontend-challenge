import 'whatwg-fetch';

const updateTransaction = ({ action }) => ({ id }) => {
  const url = `http://localhost:8080/transaction/${id}`;

  let data = null;
  if (action === 'cancel') {
    data = { status: 'CANCELLED' };
  } else if (action === 'approve') {
    data = { status: 'FL_APPROVED' };
  }

  if (data) {
    return fetch(url, {
      body: JSON.stringify(data),
      method: 'PUT'
    })
      .then(response => {
        if (!response.ok) throw new Error('Request Failed');
        return response;
      })
      .then(data => data.json());
  } else {
    throw new Error(
      'Cannot update transaction: action must be either "cancel" or "approve"'
    );
  }
};

export const cancelTransaction = updateTransaction({ action: 'cancel' });
export const approveTransaction = updateTransaction({ action: 'approve' });
