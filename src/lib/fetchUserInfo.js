import 'whatwg-fetch';

export default ({ userId }) => {
  const url = `http://localhost:8080/user/${userId}`;
  return fetch(url)
    .then(response => {
      if (!response.ok) throw new Error('Request Failed');
      return response;
    })
    .then(data => data.json());
};
