import React from 'react';
import { shallow } from 'enzyme';

import InfoBlock from '../InfoBlock';

describe('InfoBlock component', () => {
  it('displays label', () => {
    const wrapper = shallow(<InfoBlock label={'price'} value={'£3.00'} />);
    const label = wrapper.find('.info-label').text();
    expect(label).toEqual('price');
  });

  it('displays value', () => {
    const wrapper = shallow(<InfoBlock label={'price'} value={'£3.00'} />);
    const value = wrapper.find('.info-value').text();
    expect(value).toEqual('£3.00');
  });
});
