import React from 'react';
import { shallow } from 'enzyme';

import UserBlock from '../UserBlock';

const props = {
  type: 'lender',
  firstName: 'Bill',
  lastName: 'Murray',
  email: 'billmurray@gmail.com',
  telephone: '+447855555123',
  profileImgUrl: 'http://via.placeholder.com/10x10'
};

describe('UserBlock component', () => {
  it('displays a user image', () => {
    const wrapper = shallow(<UserBlock {...props} />);
    const image = wrapper.find('img[src="http://via.placeholder.com/10x10"]');
    expect(image.type()).toEqual('img');
  });

  it("displays the user's fullname", () => {
    const wrapper = shallow(<UserBlock {...props} />);
    const name = wrapper.find('.user-name').text();
    expect(name).toEqual('Bill Murray');
  });

  it("displays user's email", () => {
    const wrapper = shallow(<UserBlock {...props} />);
    const email = wrapper.find('.user-email').text();
    expect(email).toEqual('billmurray@gmail.com');
  });

  it("displays the user's telephone number", () => {
    const wrapper = shallow(<UserBlock {...props} />);
    const number = wrapper.find('.user-phone').text();
    expect(number).toEqual('+447855555123');
  });

  it('displays the lender label', () => {
    const props = {
      type: 'lender',
      firstName: 'Bill',
      lastName: 'Murray',
      email: 'billmurray@gmail.com',
      telephone: '+447855555123',
      profileImgUrl: 'http://via.placeholder.com/10x10'
    };

    const wrapper = shallow(<UserBlock {...props} />);
    const type = wrapper.find('.user-type').text();
    expect(type).toEqual('lender');
  });

  it('displays the borrower label', () => {
    const props = {
      type: 'borrower',
      firstName: 'Bill',
      lastName: 'Murray',
      email: 'billmurray@gmail.com',
      telephone: '+447855555123',
      profileImgUrl: 'http://via.placeholder.com/10x10'
    };

    const wrapper = shallow(<UserBlock {...props} />);
    const type = wrapper.find('.user-type').text();
    expect(type).toEqual('borrower');
  });

  it('should not display full name if last name is missing', () => {
    const props = {
      type: 'borrower',
      firstName: 'Bill',
      lastName: null,
      email: 'billmurray@gmail.com',
      telephone: '+447855555123',
      profileImgUrl: 'http://via.placeholder.com/10x10'
    };

    const wrapper = shallow(<UserBlock {...props} />);
    const type = wrapper.find('.user-name').text();
    expect(type).toEqual('Bill');
  });
});
