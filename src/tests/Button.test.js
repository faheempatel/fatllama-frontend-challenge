import React from 'react';
import { mount } from 'enzyme';

import Button from '../Button';

describe('Button component', () => {
  it('displays button text from prop', () => {
    const wrapper = mount(<Button text={'Test'} />);
    const button = wrapper.text();
    expect(button).toEqual('Test');
  });

  it('does a specified action on click');
});
