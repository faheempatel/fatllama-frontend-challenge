import React from 'react';
import { mount } from 'enzyme';

import StatusLabel from '../StatusLabel';

describe('StatusLabel component', () => {
  it('displays status text from prop', () => {
    const wrapper = mount(<StatusLabel status={'Approved'} />);
    const label = wrapper.find('div').text();
    expect(label).toEqual('Approved');
  });

  it('applies correct styling for approved status');
  it('applies a given priority accordingly');
});
