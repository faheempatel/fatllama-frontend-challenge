import React from 'react';
import { shallow } from 'enzyme';

import Header from '../Header';

describe('Header component', () => {
  it('displays header with title', () => {
    const wrapper = shallow(<Header title={'FatLlama Ops Dashboard'} />);
    const header = wrapper.find('div').text();
    expect(header).toEqual('FatLlama Ops Dashboard');
  });
});
