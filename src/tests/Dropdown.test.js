import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../Dropdown';

describe('Dropdown component', () => {
  it('renders options from given children', () => {
    const wrapper = shallow(
      <Dropdown>
        <option value="volvo">Volvo</option>
        <option value="saab">Saab</option>
        <option value="mercedes">Mercedes</option>
        <option value="audi">Audi</option>
      </Dropdown>
    );
    const options = wrapper.find('option');
    expect(options).toHaveLength(4);
  });

  it('does a specified action on change');
});
