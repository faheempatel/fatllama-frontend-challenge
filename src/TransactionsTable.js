import React, { Component } from 'react';
import throttle from 'lodash.throttle';

import fetchTransactions from './lib/fetchTransactions';

import TransactionRow from './TransactionRow';
import styled, { css } from 'react-emotion';

const Row = styled('div')`
  display: flex;
  align-items: center;
  width: 100%;
  padding: 8px;
  margin-left: 30px;
  > p {
    width: calc(100% / 6);
    font-weight: bold;
  }
`;

class TransactionsTable extends Component {
  state = {
    transactions: [],
    nextPage: 1,
    resultsFinished: false,
    isLoading: false
  };

  componentDidMount() {
    // For infinite scroll
    window.addEventListener('scroll', this.onScroll, false);
    this.getTransactions();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  // Throttled to avoid firing multiple requests too quickly
  onScroll = throttle(() => {
    const pageBottom =
      window.innerHeight + window.scrollY >= document.body.offsetHeight - 500;

    const pageBottomAndDataAvailable =
      pageBottom &&
      this.state.transactions.length &&
      !this.state.resultsFinished;

    if (pageBottomAndDataAvailable) {
      this.getTransactions();
    }
  }, 1000);

  getTransactions = () => {
    this.setState({ isLoading: true });

    fetchTransactions({ page: this.state.nextPage }).then(
      data => {
        setTimeout(() => {
          this.setState(prevState => ({
            transactions: [...prevState.transactions, ...data],
            nextPage: prevState.nextPage + 1,
            isLoading: false
          }));
        }, 250);
      },
      err => {
        this.setState({ isLoading: false, resultsFinished: true });
      }
    );
  };

  render() {
    return (
      <div className="inner-wrapper">
        <Row>
          <p>ID</p>
          <p>Item ID</p>
          <p>From Date</p>
          <p>To Date</p>
          <p>Status</p>
        </Row>
        <div
          className={css`
            border-top: 2px solid #e8e8e8;
          `}
        >
          {this.state.transactions.map((transaction, i) => (
            <TransactionRow key={i} transaction={transaction} />
          ))}
          {this.state.isLoading && <h3>Loading...</h3>}
        </div>
      </div>
    );
  }
}

export default TransactionsTable;
