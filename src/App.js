import React, { Component } from 'react';
import './styles/App.css';
import styled from 'react-emotion';

import Header from './Header';
import TransactionsTable from './TransactionsTable';

const Title = styled('div')`
  border-bottom: 2px solid #e5e5e4;
  margin-bottom: 16px;
  h2 {
    margin: 24px 0;
  }
`;

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header title={'FatLlama Ops Dashboard'} />
        <Title className={'inner-wrapper'}>
          <h2>Transactions</h2>
        </Title>
        <TransactionsTable />
      </div>
    );
  }
}

export default App;
