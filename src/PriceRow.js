import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

import InfoBlock from './InfoBlock';

const formatPrice = (price, currency) => {
  const formatter = new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency
  });
  return formatter.format(price);
};

const Wrapper = styled('div')`
  display: flex;
  justify-content: space-around;
  align-self: flex-end;
  width: 100%;
  padding: 24px 0;
  border-top: 1px solid #e8e8e8;
`;

const PriceRow = ({
  price,
  promocode,
  totalDiscount,
  creditUsed,
  currency
}) => {
  const total = price - totalDiscount - creditUsed;

  return (
    <Wrapper className={'price-info'}>
      <InfoBlock label={'Price'} value={formatPrice(price, currency)} />
      {promocode && <InfoBlock label={'Promocode Used'} value={promocode} />}

      <InfoBlock
        label={'Total Discount'}
        value={formatPrice(totalDiscount, currency)}
      />
      <InfoBlock
        label={'Credit Used'}
        value={formatPrice(creditUsed, currency)}
      />
      <InfoBlock label={'Total'} value={formatPrice(total, currency)} />
    </Wrapper>
  );
};

PriceRow.propTypes = {
  price: PropTypes.number.isRequired,
  promocode: PropTypes.string,
  totalDiscount: PropTypes.number,
  creditUsed: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired
};

PriceRow.defaultProps = {
  totalDiscount: 0
};

export default PriceRow;
