import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const Wrapper = styled('div')`
  line-height: 1.4;
  .info-label {
    text-transform: capitalize;
    font-size: 14px;
    color: #bfbfc3;
  }
`;

const InfoBlock = ({ label, value }) => (
  <Wrapper>
    <p className="info-label">{label}</p>
    <p className="info-value">{value}</p>
  </Wrapper>
);

InfoBlock.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired
};

export default InfoBlock;
