# FatLlama frontend coding challenge

# Approach

I approached the project by first studying the spec and playing with the API provided. Once I started to build an understanding of the data that was available I began mocking up a design ([screenshot](https://bitbucket.org/faheempatel/fatllama-frontend-challenge/raw/762128bf36d30cfef4577ac91448874e0fc83ac5/quick-mockup.png)).

With a mockup at hand I began drawing out the initial set of components taking into consideration how data might flow through the app.

I then reached a point where I questioned whether or not I would need Redux. But in order to avoid overengineering from the get go, I came to the conclusion that it would be best to make this decision upon refactoring later on.

Once I was relatively pleased with the architecture I began coding in a TDD manner. (Which I later abandoned in order to finish in the time I allocated myself).

The structure of the app was heavily influenced by the use of React. The app was split into components with a single responsibility (mostly).

# Shortcomings

- Visually, I would have liked to have spent more time making it look good

- CSS is not responsive — time permitting this is something I would improve upon

- Display info - I would like to have displayed more computed info in the dropdown, for example: number of days, etc. (I had intended to and this is why there is a big space currently there)

- Loading states - More work could have been put in to convey to user when things are loading

- Human-friendly status labels - Here I would speak to the Ops team and learn what would best make sense

- Redux - I got to a point where I starting to drill down props and this is a bit of smell for me. So this is where I would start considering Redux again

- Filtering/sorting - This was unfinished due to time. Though I had an implementation of sorting working, I left it out due to UX reasons — transactions coming from the API were unsorted from page to page which meant rows would flash around on infinite scroll.

- Robustness — I would have liked to have made the app more robust under network failures

- Finish writing tests for all components

# Libraries/Frameworks Used

- React
- [lodash.throttle](https://www.npmjs.com/package/lodash.throttle) was used to help avoid firing mulitple network requests on page scroll
- [Emotion](https://github.com/emotion-js/emotion) was used to manage styling on a component level

# Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
